#!/usr/bin/make -f

CPPFLAGS:=$(shell dpkg-buildflags --get CPPFLAGS) -DDEB_HOST_MULTIARCH=\"$(DEB_HOST_MULTIARCH)\"
CFLAGS:=$(shell dpkg-buildflags --get CFLAGS)
CXXFLAGS:=$(shell dpkg-buildflags --get CXXFLAGS)
LDFLAGS:=$(shell dpkg-buildflags --get LDFLAGS) -Wl,--as-needed

export OPENMM_INCLUDE_PATH := $(CURDIR)/openmmapi/include;$(CURDIR)/olla/include;$(CURDIR)/serialization/include;$(CURDIR)/plugins/amoeba/openmmapi/include;$(CURDIR)/plugins/rpmd/openmmapi/include;$(CURDIR)/plugins/drude/openmmapi/include
export OPENMM_LIB_PATH     := $(CURDIR)/obj-$(DEB_HOST_GNU_TYPE)

# CUDA compiler is non-free thus not available on buildds, nevertheless should not fail if present.
export OPENMM_CUDA_COMPILER := /usr/bin/nvcc

CMAKE_FLAGS = \
        -DCMAKE_VERBOSE_MAKEFILE=ON \
        -DCMAKE_C_FLAGS_RELEASE="$(CFLAGS)" \
        -DCMAKE_CXX_FLAGS_RELEASE="$(CXXFLAGS)" \
        -DCMAKE_SHARED_LINKER_FLAGS_RELEASE="$(LDFLAGS)" \
        -DCMAKE_BUILD_RPATH_USE_ORIGIN=ON \
        -DCMAKE_BUILD_TYPE=Release      \
        $(CMAKE_ARCH_FLAGS) \
        -DOPENMM_BUILD_SERIALIZATION_SUPPORT=ON \
        -DOPENMM_BUILD_C_AND_FORTRAN_WRAPPERS=OFF \
        -DOPENMM_BUILD_OPENCL_TESTS=OFF \

%:
	dh $@ --buildsystem cmake --with numpy3,python3

override_dh_auto_configure:
	dh_auto_configure -- $(CMAKE_FLAGS)

execute_after_dh_auto_build:
	cd obj-*/python && python3 setup.py build

execute_after_dh_auto_test:
	cd obj-*/python/tests && PYTHONPATH=$$(realpath ../build/lib.linux-*) pytest-3

override_dh_auto_install:
	dh_auto_install -O--buildsystem=cmake
	dh_install -plibopenmm8.0      debian/tmp/usr/lib/libOpenMM.so.* usr/lib/${DEB_HOST_MULTIARCH}
	dh_install -plibopenmm-dev     debian/tmp/usr/lib/libOpenMMAmoeba.so usr/lib/${DEB_HOST_MULTIARCH}/openmm
	dh_install -plibopenmm-dev     debian/tmp/usr/lib/libOpenMMDrude.so  usr/lib/${DEB_HOST_MULTIARCH}/openmm
	dh_install -plibopenmm-dev     debian/tmp/usr/lib/libOpenMMRPMD.so   usr/lib/${DEB_HOST_MULTIARCH}/openmm
	dh_install -plibopenmm-dev     debian/tmp/usr/lib/libOpenMM.so   usr/lib/${DEB_HOST_MULTIARCH}
	dh_install -plibopenmm-plugins debian/tmp/usr/lib/plugins        usr/lib/${DEB_HOST_MULTIARCH}/openmm
	cd obj-*/python && python3 setup.py install --install-layout=deb --root=$(CURDIR)/debian/tmp

override_dh_python3:
	dh_python3
	find debian/python3-openmm -name '*.py' \
		| xargs sed -i 's/^#!\/bin\/env python/#!\/usr\/bin\/python3/'
	find debian/python3-openmm -name __pycache__ \
		| xargs rm -rf
	find debian/python3-openmm -name '_openmm.*.so' \
		| xargs chrpath --replace /usr/lib/${DEB_HOST_MULTIARCH}/openmm

override_dh_shlibdeps:
	dh_shlibdeps --dpkg-shlibdeps-params=--ignore-missing-info
